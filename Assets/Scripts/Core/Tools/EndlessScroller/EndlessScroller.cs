﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessScroller : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] Vector3 _scrollVelocity;
    [SerializeField] Vector3 _maxBoundary;
    [SerializeField] Vector3 _startingSpawnLocation;
    [SerializeField] [Range(0, 1)] float _adjustment;
    [Header("References")]
    [SerializeField] List<GameObject> _scrollingObjects;
    List<float> _distancesFromBoundary = new List<float>();
    private void Start()
    {
        SetScrollingObjectsStartingPosition();
    }

    private void Update()
    {
        for (int i = 0; i < _scrollingObjects.Count; i++)
        {
            GameObject scrollingObject = _scrollingObjects[i];
            scrollingObject.transform.position += (Time.deltaTime * _scrollVelocity);
            float distanceFromBoundary = GetDistanceFromBoundary(scrollingObject);
            if (distanceFromBoundary > _distancesFromBoundary[i])
            {
                SetObjectPositionToEnd(scrollingObject);
                _distancesFromBoundary[i] = GetDistanceFromBoundary(scrollingObject);
            }
            else
            {
                _distancesFromBoundary[i] = distanceFromBoundary;
            }
        }
    }

    private void SetScrollingObjectsStartingPosition()
    {
        for (int i = 0; i < _scrollingObjects.Count; i++)
        {
            GameObject scrollingObject = _scrollingObjects[i];
            if (i == 0)
            {
                scrollingObject.transform.position = _startingSpawnLocation;
            }
            else
            {
                SetObjectPositionToEnd(scrollingObject);
            }
            _distancesFromBoundary.Add(GetDistanceFromBoundary(scrollingObject));
        }
    }

    private float GetDistanceFromBoundary(GameObject p_scrollingObject)
    {
        return Vector2.Distance(p_scrollingObject.transform.position, _maxBoundary);
    }

    private void SetObjectPositionToEnd(GameObject p_gameObject)
    {
        int gameobjectIndex = _scrollingObjects.IndexOf(p_gameObject);
        int previousGameObjectIndex = gameobjectIndex - 1;
        if (gameobjectIndex == 0)
        {
            previousGameObjectIndex = _scrollingObjects.Count - 1;
        }
        GameObject previousGameobject = _scrollingObjects[previousGameObjectIndex];
        BoxCollider2D previousGoBoxCollider = previousGameobject.GetComponent<BoxCollider2D>();
        p_gameObject.transform.position = (Vector2)previousGameobject.transform.position + (new Vector2(previousGoBoxCollider.size.x * previousGameobject.transform.localScale.x, previousGoBoxCollider.size.y * previousGameobject.transform.localScale.y) * -_scrollVelocity.normalized * _adjustment);
    }
}
