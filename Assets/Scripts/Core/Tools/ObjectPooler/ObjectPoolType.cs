﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Object Pool Type", menuName = "Object Pool Type")]
public class ObjectPoolType : ScriptableObject
{
    [Header("Settings")]
    [SerializeField] string _objectPoolName;
    [SerializeField] ObjectPoolSettings _objectPoolSettings;
    [Header("References")]
    [SerializeField] List<ObjectPoolSubscriber> _subscribers = new List<ObjectPoolSubscriber>();
    [SerializeField] ObjectPool _objectPool;

    public ObjectPool RegisterSubscriber(ObjectPoolSubscriber p_subscriber)
    {
        if (!_subscribers.Contains(p_subscriber))
        {
            _subscribers.Add(p_subscriber);
            CreateOrIncreasePool();
        }
        return _objectPool;
    }

    public void UnregisterSubscriber(ObjectPoolSubscriber p_subsriber)
    {
        if (_subscribers.Contains(p_subsriber))
        {
            _subscribers.Remove(p_subsriber);
        }
    }

    private void CreateOrIncreasePool()
    {
        // If object pool does not exist, create one
        if (_objectPool == null)
        {
            //Create ObjectPools parent container
            GameObject parent = GameObject.Find("ObjectPools");
            if (parent == null)
            {
                parent = new GameObject("ObjectPools");
            }
            _objectPool = new GameObject().AddComponent<ObjectPool>();
            //Set Object Pool Name
            string objectPoolName = _objectPoolName;
            if (objectPoolName == "")
            {
                objectPoolName = _objectPoolSettings.prefab.name + "Pool";
            }
            _objectPool.name = objectPoolName;
            //Organize Object Pool under ObjectPools
            _objectPool.gameObject.transform.parent = parent.transform;
            //Initialize and Create Object Pool
            _objectPool.SetPoolSettings(_objectPoolSettings);
            _objectPool.CreatePool();
        }
        // If object pool exists, increase its size
        else
        {
            // Check if object pool needs to be increased
            if ((_subscribers.Count * _objectPoolSettings.prefabCountPerSubscriber) > _objectPool.poolSize)
            {
                _objectPool.IncreasePool(_objectPoolSettings.prefabCountPerSubscriber);
            }
        }
    }
}
