﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolSubscriber : MonoBehaviour
{
    [SerializeField] ObjectPoolType _objectPoolType;
    [SerializeField] ObjectPool _objectPool;

    public ObjectPool objectPool { get { return _objectPool; } }
    private void OnEnable()
    {
        RegisterSubsriber();
    }

    private void OnDestroy()
    {
        UnregisterSubsriber();
    }

    public void RegisterSubsriber()
    {
        if (_objectPoolType)
        {
            _objectPool = _objectPoolType.RegisterSubscriber(this);
        }
    }

    public void UnregisterSubsriber()
    {
        if (_objectPoolType)
        {
            _objectPoolType.UnregisterSubscriber(this);
        }
    }

    public void SetObjectPoolType(ObjectPoolType p_objectPoolType)
    {
        _objectPoolType = p_objectPoolType;
    }
}
