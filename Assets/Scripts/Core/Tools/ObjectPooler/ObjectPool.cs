﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] GameObject _prefab;
    [SerializeField] int _prefabCount = 0;
    [SerializeField] Vector3 _poolPosition;

    List<GameObject> _pool;
    bool _poolInitialized;

    public int poolSize { get { return _pool.Count; } }

    public void SetPoolSettings(ObjectPoolSettings p_settings)
    {
        _prefab = p_settings.prefab;
        _prefabCount = p_settings.prefabCountPerSubscriber;
        _poolPosition = p_settings.poolPosition;
    }

    public void CreatePool()
    {
        _pool = new List<GameObject>();
        for (int i = 0; i < _prefabCount; i++)
        {
            GameObject item = Instantiate(_prefab, _poolPosition, Quaternion.identity);
            AddItemToPool(item);
        }
    }

    public void IncreasePool(int p_amount)
    {
        for (int i = 0; i < p_amount; i++)
        {
            GameObject item = Instantiate(_prefab, _poolPosition, Quaternion.identity);
            AddItemToPool(item);
        }
    }

    public GameObject GetItem()
    {
        GameObject item = _pool[0];
        _pool.RemoveAt(0);
        return item;
    }

    public void AddItemToPool(GameObject p_item)
    {
        p_item.SetActive(false);
        p_item.transform.position = _poolPosition;
        p_item.transform.parent = gameObject.transform;
        _pool.Add(p_item);
    }
}

[System.Serializable]
public class ObjectPoolSettings
{
    [SerializeField] GameObject _prefab;
    [SerializeField] int _prefabCountPerSubscriber;
    [SerializeField] Vector3 _poolPosition;
    GameObject _parentContainer;

    public GameObject prefab { get { return _prefab; } }
    public int prefabCountPerSubscriber { get { return _prefabCountPerSubscriber; } }
    public Vector3 poolPosition { get { return _poolPosition; } }
}
