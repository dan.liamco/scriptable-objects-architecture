﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTester : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField] StringVariable _myName;
    [SerializeField] IntVariable _myAge;
    [SerializeField] FloatVariable _myPretendAge;

    [Header("Unity Events")]
    [SerializeField] VoidUnityEvent _voidEvent;
    [SerializeField] IntUnityEvent _intEvent;
    [SerializeField] FloatUnityEvent _floatEvent;
    [SerializeField] StringUnityEvent _stringEvent;

    [Header("SO Events")]
    [SerializeField] VoidEvent _SoVoidEvent;
    [SerializeField] IntEvent _SoIntEvent;
    [SerializeField] FloatEvent _SoFloatEvent;
    [SerializeField] StringEvent _SoStringEvent;

    int _voidEventRaised = 0;

    private void Start()
    {
        //Invoke SO Events
        _SoVoidEvent.Raise();
        _SoIntEvent.Raise(_myAge.value);
        _SoFloatEvent.Raise(_myPretendAge.value);
        _SoStringEvent.Raise(_myName.value);
        
        /*
        //Invoke Unity Events
        _floatEvent.Invoke(_myPretendAge.value);
        _intEvent.Invoke(_myAge.value);
        _voidEvent.InvokeVoid();
        _stringEvent.Invoke(_myName.value);
        */
    }

    public void OnFloatEventRaised(float p_float)
    {
        print(p_float);
    }

    public void OnIntEventRaised(int p_int)
    {
        print(p_int);
    }

    public void OnVoidEventRaised()
    {
        _voidEventRaised++;
        print("Void Events Raised: " + _voidEventRaised);
    }

    public void OnStringEventRaised(string p_string)
    {
        print(p_string);
    }
}
